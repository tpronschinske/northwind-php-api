<?php

namespace Controllers;

use Exception;
use Core\Controller;
use \Helpers\Request;
use \Middleware\ResponseMiddleware;

class EmployeeController extends Controller
{

  private $_employee;

  public function __construct()
  {
      parent::__construct();
      $this->$_employee = new \Service\EmployeeService();
  }


  public function getEmployees(){
    try {
        $employees = $this->$_employee->getEmployees();
        if (isset($employees) && count($employees) > 0) {
          ResponseMiddleware::SuccessJsonResponse($employees);
        } else {
          throw new Exception('Employees Not Found', 404);
        }
    } catch (Exception $e) {
        ResponseMiddleware::ErrorJsonResponse($e);
    }
  }

  public function getEmployeeById($id){
    try {
        $employee = $this->$_employee->getEmployeeById($id);
        if (isset($employee) && count($employee) > 0) {
           ResponseMiddleware::SuccessJsonResponse($employee);
        } else {
           throw new Exception('Employee Not Found Exception', 404);
        }
    } catch (Exception $e) {
        ResponseMiddleware::ErrorJsonResponse($e);
    }
  }

  public function createEmployee(){
    try {
        $postData = json_decode($_POST['data']);
        if(Request::isPost()){
          if(isset($postData->FirstName) && isset($postData->LastName)){
            $this->$_employee->createEmployee($postData);
            ResponseMiddleware::SuccessJsonResponse($postData, 'Employee was created successfully.');
          } else {
            throw new Exception('Invalid Model Exception', 500);
          }
        }
    } catch (Exception $e) {
        ResponseMiddleware::ErrorJsonResponse($e);
    }
  }

  public function updateEmployee(){
    try {
        $postData = json_decode($_POST['data']);
        if(Request::isPut()){
          if(isset($postData->FirstName) && isset($postData->LastName)){
            $this->$_employee->updateEmployee($postData);
            ResponseMiddleware::SuccessJsonResponse($postData, 'Employee was updated successfully.');
          } else {
            throw new Exception('Invalid Model Exception', 500);
          }
        }
    } catch (Exception $e) {
        ResponseMiddleware::ErrorJsonResponse($e);
    }
  }


}
