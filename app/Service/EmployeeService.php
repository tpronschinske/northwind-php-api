<?php

namespace Service;

class EmployeeService extends \Core\Service {

  public function getEmployees(){
      return $this->db->select("SELECT * FROM " . PREFIX . "employees");
  }

  public function getEmployeeById($id) {
      return $this->db->select("SELECT * FROM " . PREFIX . "employees WHERE EmployeeID=:EmployeeID", array(':EmployeeID' => $id));
  }

  public function createEmployee($data) {
    $employeeData = array(
      'FirstName' => $data->FirstName,
      'LastName' => $data->LastName,
      'Title' => $data->Title,
      'BirthDate' => $data->BirthDate,
      'HireDate' => $data->HireDate,
      'Address' => $data->Address,
      'City' => $data->City,
      'Region' => $data->Region,
      'PostalCode' => $data->PostalCode,
      'Country' => $data->Country,
      'HomePhone' => $data->HomePhone,
      'Extension' => $data->Extension,
      'Photo' => $data->Photo,
      'Notes' => $data->Notes,
      'ReportsTo' => $data->ReportsTo,
      'PhotoPath' => $data->PhotoPath,
      'Salary' => $data->Salary,
    );
    $this->db->insert('employees', $employeeData);
  }

  public function updateEmployee($data){
		$where = array('Id' => $data->EmployeeID);
    $employeeUpdate = array(
      'FirstName' => $data->FirstName,
      'LastName' => $data->LastName,
      'Title' => $data->Title,
      'BirthDate' => $data->BirthDate,
      'HireDate' => $data->HireDate,
      'Address' => $data->Address,
      'City' => $data->City,
      'Region' => $data->Region,
      'PostalCode' => $data->PostalCode,
      'Country' => $data->Country,
      'HomePhone' => $data->HomePhone,
      'Extension' => $data->Extension,
      'Photo' => $data->Photo,
      'Notes' => $data->Notes,
      'ReportsTo' => $data->ReportsTo,
      'PhotoPath' => $data->PhotoPath,
      'Salary' => $data->Salary,
    );
		return $this->db->update(PREFIX . "employees", $employeeUpdate, $where);
	}


}
